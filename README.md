# Image + File viewer

![ImageWalker](src/interface.png)

## Installation

``` console
~$ pip install -e .
```

## Update

To update the package, pull from the master branch.
The is no need to re

``` console 
~$ git pull origin master
```

## Usage

- Run without argment, this will open the session at the current folder:
``` console
~$ walker
```

- Run with input image or folder `imwalker`:
``` console
~$ imwalker path_to_image_file
```

## Option: 

- ~~`-d`, `--debug` : run in debug mode (detail error, support pdb, ... etc)~~
  - autorun `app` in debug mode to prevent port dropping bug.
- `-t`, `--title` : the tab title 
- `-p`, `--port` : change the number of port (default: 8050)


## Troubleshooting

The application is built using [dash](https://dash.plotly.com/) (by [plotly](https://plotly.com/)), which is structured similar to a web application.
As the web browsers handle things differently, this may results in unexpected behaviour in some browsers.
Although, the changes might not break the application, simply running [localhost:8050](localhost:8050) on a different browser might solve the bug.

The current version was built and debugged on [brave](https://brave.com/) browser (under the [chromium](https://www.chromium.org/) family).
I will run tests with `firefox`.


## Known Bug

|Bug/Bug Msg| Details |
|---|---|
|Nonexisten object was used... | *This error can be ignored (by clicking the `x` in the top right cornor of the error dialog).* Clicking the `Folder Up` button, when in the sidebar table is in `Image Table` mode, will move the current directory up one level and automatically change the sidebar to the `File Table` mode. The error is shown because the `File Table` was not rendered properly, when all the changes happend. |


## Video Demo (click on the image)

* [First Step: Finding Image](#first-step-finding-image)
* [Basic Viewer Controls](#basic-viewer-controls)
* [Reading Dicom Files](#reading-dicom-files)

### First Step: Finding Image

<a href="demo/finding-image.mp4" download="finding-image.mp4">![finding-image](demo/finding-image.png)</a>

### Basic Viewer Controls

<a href="demo/viewer-control.mp4" download="viewer-control.mp4">![viewer-control](demo/viewer-control.png)</a>

### Reading Dicom Files

<a href="demo/reading-dicom.mp4" download="reading-dicom.mp4">![reading-dicom](demo/reading-dicom.png)</a>


