import os
import glob
import pdb

import numpy as np
import SimpleITK as sitk
import pydicom


sitk_readable_ext = {
".mha", ".mhd", # MetaImageIO
".nia", ".nii", ".nii.gz", ".hdr", ".img", ".img.gz", # NiftiImageIO
".nrrd", ".nhdr", # NrrdImageIO
".tif", ".TIF", ".tiff", ".TIFF", # TIFFImageIO
}

# TODO: add RGB image
# ".bmp", ".BMP", # BMPImageIO
# ".jpg", ".JPG", ".jpeg", ".JPEG", # JPEGImageIO
# ".png", ".PNG", # PNGImageIO

def get_abspath(path):
    """
    """
    return os.path.abspath(os.path.expanduser(path))

def get_basename(path):
    """
    """
    return os.path.basename(path).split('.')[0]


def read_dicom(path):
    """
    """
    reader = sitk.ImageSeriesReader()
    reader.SetFileNames(path)
    img = reader.Execute()
    spacing = list(img.GetSpacing())[::-1]
    img_np = sitk.GetArrayFromImage(img)
    return img_np, spacing


def read_sitk(path):
    """
    """
    img = sitk.ReadImage(path)
    spacing = list(img.GetSpacing())[::-1]
    img = sitk.GetArrayFromImage(img) 
    title = get_basename(path)
    return img, spacing, title


def read_npy(path):
    """
    """
    val = np.load(path)
    basename = get_basename(path)

    if val.ndim <= 3:
        img = val
        spacing = [1] * val.ndim
        title = basename

    elif val.ndim == 4: # break 4 dim array to seperate imgs or display as time frame img
        shape = val.shape
        axis = 0 if shape[0] < shape[-1] else 1 # switch first and last dim
        # time frame image limit at >10 frame
        val = np.swapaxes(val, axis, 0)
        if shape[axis] > 10: # store image as time frame image
            img = val
            spacing = [1, 1, 1]
            title = basename

        else: 
            img = [val[ind] for ind in range(shape[0])] # save as list of 3d array
            title = [f"{basename}_{ind}" for ind in range(shape[0])]
            spacing = [[1,1,1] for i in range(len(img))]

    elif val.ndim == 5: # break image by batch and channel
        shape = val.shape
        img = []
        title = []
        for b in range(shape[0]):
            img.extend([val[b][..., c] for c in range(shape[-1])]) # used channel last (tensorflow/keras) convention.
            title.extend([f'{basename}_{b}{c}' for c in range(shape[-1])])

            # TODO: add channel first convention (for torch support)
        spacing = [[1,1,1] for i in range(len(img))]
    return img, spacing, title


def read_npz(path):
    """
    """
    pkg = np.load(path, allow_pickle=True)
    img = []
    title = []
    for key, val in pkg.items(): 
        if (val.ndim == 2) or (val.ndim == 3):
            img.append(val)
            title.append(key)

        elif val.ndim == 4:
            shape = val.shape
            axis = 0 if shape[0] < shape[-1] else 1
            # time frame image limit at >10 frame
            val = np.swapaxes(val, axis, 0)
            if shape[axis] > 10:
                # move axis
                img.append(val)
                title.append(key)
            else: 
                img.extend([val[ind] for ind in range(shape[0])])
                title.extend(f"{key}_{ind}" for ind in range(shape[0]))

        elif val.ndim == 5:
            shape = val.shape
            if shape[-1] < shape[1]: # second axis
                val = np.swapaxes(val, -1, 1)
                shape = val.shape

            if shape[1] > 10: # store as animation
                img.extend([val[b]] for b in range(shape[0]))
                title.extend(f"{key}_{ind}" for ind in range(shape[0]))
            else:
                for b in range(shape[0]):
                    img.extend([val[b][chn] for chn in range(shape[1])])
                    title.extend([f"{key}_{b}{chn}" for chn in range(shape[1])])

    spacing = [[1,1,1]] * len(img)
    return img, spacing, title


def parse_dcm_file(dirname):
    """
    """
    series_reader = sitk.ImageSeriesReader()
    _series_id = series_reader.GetGDCMSeriesIDs(dirname) # using GDCM reader miss the series name
    # files = glob.glob(os.path.join(dirname, "*.dcm"))
    # _series_id = list(set([os.path.basename(_file).split("_")[0] for _file in files]))
    desc = [] 
    path = []
    for _id in _series_id: 
        _file = series_reader.GetGDCMSeriesFileNames(dirname, seriesID=_id)

        # _file = glob.glob(os.path.join(dirname, f"{_id}*"))
        # _file.sort()
        # _file = _file[::-1] # reverse order

        ds = pydicom.dcmread(_file[0])
        # try: 
        _desc = getattr(ds, 'SeriesDescription')
        # else: 
        #     _desc = _id
        desc.append(_desc)
        path.append(_file)
    return desc, path


def ravel(nested_list: list)->list:
    """ ravel all level of nested_list.
    """
    while any([isinstance(i, (list, tuple)) for i in nested_list]): 
        new_ = []
        for item in nested_list: 
            if isinstance(item, (list, tuple)):
                new_.extend(item)

            else: 
                new_.append(item)
        nested_list = new_
    return nested_list
