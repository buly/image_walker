""" Set up layout component.
"""
import pdb
import os

import dash 
import dash_table
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
# import dash_vtk
# from dash_vtk.utils import presets
# from dash_vtk.utils import to_mesh_state
from dash.exceptions import PreventUpdate
import plotly
import plotly.express as px
import plotly.graph_objects as go


def get_file_viewer_table(data=None,
        column='filename'):
    """
    """
    if data is None:
        data = []
    elif not isinstance(data, (list, tuple)): 
        data = [data]

    # process filename
    # data = [{col: val for val, col in zip(row, columns) } for row in data]
    data = [{column: row} for row in data]

    table = dash_table.DataTable(id='file-table',
        columns=[{"name": column, "id": column}],
        data=data,
        style_cell={
            'textAlign':'left', 
            # 'font-size':'17px',
            },
        style_header=dict(
            backgroundColor="#A3B4C3", 
            fontWeight='bold'),
        style_table={
            'overflowY': 'auto',
            'overflowX': 'auto',
            'min-width': '100%',
            # 'min-height': '100vh',
            'height': '90vh',
            'border':"1px solid pink",
            },
        # fixed_rows={'headers': True},
        )
    return table


def get_image_file_table(df,
        column='id',
        ):
    """
    """
    if df is None:
        data=[]
    else: 
        data=df[['id']].to_dict('records')

    table = dash_table.DataTable(id='image-table',
        columns=[{"name": column, "id": column}],
        data=data,
        style_header=dict(
            backgroundColor="#FBAB81", 
            fontWeight='bold'),
        style_cell={
            'textAlign':'left', 
            # 'font-size':'17px',
            },
        style_table={
            'overflowY': 'auto',
            'overflowX': 'auto',
            'min-width': '100%',
            # 'min-height': '100vh',
            # 'min-height': '100%',
            'height': '90vh',
            'border':"1px solid pink",
            },
        # fixed_rows={'headers': True},
        row_selectable="single",
        )
    return table
