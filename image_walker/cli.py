import os
import pdb
import argparse
import configparser

from . import image_walker
from . import utils as Utils


def cli_walker():
    """
    """ 
    def getargs():
        """ CLI arguments
        """
        parser = argparse.ArgumentParser(description="", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    
        parser.add_argument('--debug', '-d',
                    action='store_true',
                    help=""" Run app in debug mode. """, 
                    )

        parser.add_argument('--port', '-p',
                    action='store',
                    help=""" Run app in debug mode. """, 
                    type=int,
                    default=8050,
                    )

        parser.add_argument('--title', '-t',
                    action='store',
                    help=""" Change the title show in web browser tab. """, 
                    type=str,
                    default='ImageWalker',
                    )

        return parser.parse_args()

    args = getargs()
    debug = True # args.debug
    port = args.port

    walker = image_walker.ImageWalker(homedir='')
    walker.deploy(title=args.title)
    walker.app.run_server(debug=debug,
            port=port,)


def cli_imwalker():
    """
    """ 
    def getargs():
        """ CLI arguments
        """
        parser = argparse.ArgumentParser(description="", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    
        parser.add_argument('input',
                    action='store',
                    nargs='+',
                    help="""  """, 
                    # type=str,
                    )

        parser.add_argument('--debug', '-d',
                    action='store_true',
                    help=""" Run app in debug mode. """, 
                    )

        parser.add_argument('--port', '-p',
                    action='store',
                    help=""" Run app in debug mode. """, 
                    type=int,
                    default=8050,
                    )

        parser.add_argument('--title', '-t',
                    action='store',
                    help=""" Change the title show in web browser tab. """, 
                    type=str,
                    default='ImageWalker',
                    )
        return parser.parse_args()

    args = getargs()
    debug = True # args.debug
    port = args.port

    inputs = args.input

    if len(inputs) > 1: 
        raise NotImplementError

    else: 
        _input = Utils.get_abspath(inputs[0])

        if os.path.isdir(_input): 
            homedir = _input
            filepath = None

        else: 
            homedir = os.path.abspath(os.path.join(_input, ))
            filepath = _input

        walker = image_walker.ImageWalker(homedir=homedir)
        walker.deploy(title=args.title)
        if filepath is not None: 
            walker.force_image_selection(filepath)
        else:
            walker.force_parse_image_file(homedir)

    walker.app.run_server(debug=debug,
            port=port,)
