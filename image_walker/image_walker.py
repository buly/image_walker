import re
import pdb
import glob
import os
from collections import Counter
import json
import pathlib

import numpy as np
import pandas as pd
import SimpleITK as sitk

import dash 
import dash_table
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State, ALL, MATCH
# import dash_vtk
# from dash_vtk.utils import presets
# from dash_vtk.utils import to_mesh_state
from dash.exceptions import PreventUpdate
import plotly
import plotly.express as px
import plotly.graph_objects as go

from . import layout as Layout
from . import utils as Utils
from . import imshow as Imshow


class ImageWalker(object):
    def __init__(self,
            homedir='~', 
            ):
        """
        """
        self.homedir = Utils.get_abspath(homedir)
        self.parse_dir = None
        self.image_df = pd.DataFrame()
        self.file_df = pd.DataFrame()

        self.img_viewer = Imshow.Imshow()
        self.build()

        self.force_find_image_triggerd = False


    def get_directory_row(self,):
        """
        """
        datalist = html.Datalist(id='suggestion-path', dir='rtl')
        self.current_dir_input = dbc.Input(id='current-dir',
                                        value=self.homedir,
                                        list='suggestion-path',
                                        type='text',
                                        )

        self.find_image_button = dbc.Button("Find Image",
                                    id='find-image-button',
                                    color='primary',
                                    )

        self.back_dir_button = dbc.Button(
                                    "⬆",
                                    # "⤺",
                                    id='back-dir-button',
                                    color='info',
                                    )
        
        current_dir_input_group = dbc.InputGroup(
                [
                    dbc.InputGroupAddon(
                        self.back_dir_button,
                        addon_type='prepend',
                        ),
                    self.current_dir_input,
                    datalist,
                    dbc.InputGroupAddon(
                        self.find_image_button,
                        addon_type='append',
                        ),
                    ]
                )
        return current_dir_input_group


    def build(self, ):
        """
        """
        directory_row = self.get_directory_row()

        img_card = dbc.Card(id='img-card', 
                children=html.Div(self.img_viewer.layout, style=dict(display='none')),
                style=dict(
                    width='75vw', 
                    hight='90vh', 
                    )
                )

        self.file_table = Layout.get_file_viewer_table()
        self.image_table = Layout.get_image_file_table(None)

        self.img_selector_mode = dbc.Select(id='img-selector-mode',
                options=[{'label':key, 'value': key} for key in ['single', 'multi']],
                value='single',
                )

        display_row = dbc.Row(
                children=[
                    dbc.Col(
                        children=[
                            dbc.Row(
                                self.img_selector_mode,
                                ),
                            dbc.Row(
                                    html.Div('filetable',
                                        id='sidebar-div',
                                        style={
                                            'min-width': '20vw',
                                            'width': '20vw',
                                            }
                                        ),
                                ),
                            ],
                        style={'size':2},
                        ),
                    dbc.Col(
                        children=[
                            img_card,
                            ],
                        style={'size':10},
                        ),
                    ],
                )

        self.layout = dbc.Container(
                style=dict(minWidth='95vw',
                    maxHeight='95vh'),
                children=[
                    html.Div(
                        children=[self.file_table, self.image_table],
                        style=dict(display='none'),
                        ),
                    dbc.Row(directory_row,),
                    dbc.Row(display_row),
                    ],
                )


    def update_image_file_table(self, 
            image_df=None, 
            column='id'):
        """
        """
        if image_df is None: 
            image_df = self.image_df

        data = image_df[['id']].to_dict('records')
        self.image_table.columns = [{"name": column, "id": column}]
        self.image_table.data = data


    def update_file_viewer_table(self, 
            # data, 
            current_dir,
            column='name'
            ):
        """
        """
        self.parse_file(current_dir)
        data = self.file_df.to_dict('records')# [{column: row} for row in data]
        columns = [{"name": 'name', 'id': 'name', 'type': 'text'},]

        style_data_conditional=[
                {
                    'if': {
                        "filter_query":"{dir}=1",
                        'column_id': 'name',
                        },
                    'textDecoration':'underline',
                    'color': 'blue',
                    },
                {
                    'if': {
                        "filter_query":"{dir}=0",
                        'column_id': 'name',
                        },
                    'backgroundColor':'#cccccc',
                    'cursor':'not-allowed',
                    }
                ]

        self.file_table.columns=columns 
        self.file_table.data=data
        self.file_table.style_data_conditional=style_data_conditional


    def deploy(self, app=None, title='ImageWalker'):
        """
        """
        self.app = app
        if self.app is None:
            external_stylesheets = [dbc.themes.JOURNAL]
            self.app = dash.Dash(__name__, 
                    external_stylesheets=external_stylesheets, 
                    suppress_callback_exceptions=True,
                    title=title,
                    )
            self.app.layout = self.layout
        self.img_viewer.deploy(self.app) 


        @self.app.callback(
                Output('current-dir', 'value'),
                Input('file-table', 'active_cell'),
                Input('back-dir-button', 'n_clicks'),
                State('current-dir', 'value'),
                )
        def update_current_dir(active_cell, n_clicks, current_dir):
            """
            """
            ctx = dash.callback_context
            button_triggered = False if not ctx.triggered\
                    else ctx.triggered[0]['prop_id'].split('.')[0] == 'back-dir-button'

            if button_triggered:
                pardir = os.path.abspath(os.path.join(current_dir, os.path.pardir))
                return pardir

            else:
                if active_cell is None:
                    raise PreventUpdate

                path = self.file_df.iloc[active_cell['row']]['path']
                if os.path.isdir(path): 
                    return path

                else:
                    raise PreventUpdate
            

        @self.app.callback(
                Output('suggestion-path', 'children'),
                Input('current-dir', 'value'),
                )
        def update_suggestion_path(input_dir,):
            """
            """
            suggestion_limit = 20
            if input_dir is None: 
                raise PreventUpdate 

            find_dir = lambda paths: [path for path in paths if os.path.isdir(path)]
            current_dir_list = find_dir(glob.glob(f"{input_dir}*"))
            next_dir_list = find_dir(glob.glob(os.path.join(f"{input_dir}", '*')))

            if len(current_dir_list) > 1:
                current_dir_list.sort()
                suggestion_paths = [html.Option(value=path)
                        for path in current_dir_list[:suggestion_limit]]
                return suggestion_paths

            elif len(next_dir_list) > 0:
                next_dir_list.sort()

                suggestion_paths = [html.Option(value=path) 
                        for path in next_dir_list[:suggestion_limit]]
                return suggestion_paths

            else: 
                raise PreventUpdate 


        @self.app.callback(
                Output('current-dir', 'valid'),
                Output('current-dir', 'invalid'),
                Output('sidebar-div', 'children'),
                Input('find-image-button', 'n_clicks'),
                Input('current-dir', 'value'),
                Input('back-dir-button', 'n_clicks'),
                )
        def update_sidebar(n_clicks, current_dir, back_n_clicks):
            """
            """
            ctx = dash.callback_context
            button_triggered = False if not ctx.triggered\
                    else ctx.triggered[0]['prop_id'].split('.')[0] == 'find-image-button'
            # force parse if the button is clicked
            if button_triggered or self.force_find_image_triggerd:
                self.force_find_image_triggerd = False
                if n_clicks is None: 
                    raise PreventUpdate
                self.parse_image_file(current_dir, force=button_triggered) 
                valid = os.path.isdir(current_dir) and (len(self.image_df) > 0)
                if valid:
                    self.update_image_file_table()
                    table = self.image_table 

                else:
                    self.update_file_viewer_table(current_dir)
                    table = self.file_table
                return valid, not valid, table

            else:
                valid = False
                invalid = False

                if current_dir is None: 
                    raise PreventUpdate 

                self.update_file_viewer_table(current_dir)
                table = self.file_table
                return False, False, table 

        @self.app.callback(
                Output('image-table', 'row_selectable'),
                Input('img-selector-mode', 'value'),
                )
        def update_row_selectable(selector_mode):
            """
            """
            if selector_mode is None:
                raise PreventUpdate
            row_selectable = 'single'  if selector_mode == 'single' else 'multi'
            return row_selectable

        @self.app.callback(
                Output('img-card', 'children'),
                Input('image-table', 'derived_virtual_selected_rows'),
                State('image-table', 'derived_virtual_indices'),
                )
        def update_image_card(row_index, 
                df_index, 
                ):
            """
            """
            if not row_index: 
                raise PreventUpdate

            img = []
            spacing = []
            title = []
            path = []
            for _index in row_index:
                _index = df_index[_index]
                _img_path = self.image_df['path'].iloc[_index]
                _img_type = self.image_df['type'].iloc[_index]

                _img, _spacing, _title, _img_path = self.load_image(_index, df=self.image_df)
                if isinstance(_img, (list, tuple)):
                    img.extend(_img)
                    spacing.extend(_spacing)
                    title.extend(_title)
                    path.extend(_img_path)

                else:
                    img.append(_img)
                    spacing.append(_spacing)
                    title.append(_title)
                    path.append(_img_path)

            self.img_viewer.update(image=img, 
                    spacing=spacing, 
                    title=title, 
                    path=path,
                    )
            return self.img_viewer.layout 


    def load_image(self, 
            index, 
            df):
        """
        """
        path = df.iloc[index]['path']
        _type = df.iloc[index]['type']

        if _type == 'sitk': 
            img, spacing, title = Utils.read_sitk(path)

        if _type == 'dcm':
            img, spacing = Utils.read_dicom(path)
            title = df.iloc[index]['id']

        elif _type == 'npy':
            img, spacing, title = Utils.read_npy(path)

        elif _type == 'npz':
            img, spacing, title = Utils.read_npz(path)

        # elif _type == 'vtk': 
        #     img = pv.read(path)
        #     title = Utils.get_basename(path)

        return img, spacing, title, path


    def force_image_selection(self, 
            filepath):
        """
        """
        pardir = os.path.abspath(os.path.join(filepath, os.pardir))
        self.parse_image_file(pardir)

        df = self.image_df[self.image_df['path'].apply(os.path.basename) == os.path.basename(filepath)]
        if len(df) > 0:
            self.image_table.selected_rows = list(df.index)
        self.force_parse_image_file(pardir)


    def force_parse_image_file(self, 
            parse_dir=None, 
            ):
        """ Force image scanning.
        """
        if parse_dir is not None:
            self.current_dir_input.value = parse_dir

        # trigger clicks; trigger UI change the same way as clicking Find image
        self.force_find_image_triggerd = True
        if getattr(self.find_image_button, 'n_clicks', None) is not None:
            self.find_image_button.n_clicks += 1

        else:
            setattr(self.find_image_button, 'n_clicks', 1)


    def parse_file(self, parse_path): 
        """ Update file_df. Find and list all file and folder.
        """
        if os.path.isdir(parse_path):
            paths= glob.glob(os.path.join(parse_path, '*'))
        else:
            paths= glob.glob(parse_path+'*')
        if len(paths) > 100: 
            paths = paths[:100]

        file_df = pd.DataFrame()
        file_df['path'] = paths
        file_df['name'] = file_df['path'].apply(os.path.basename)
        def isdir(path): 
            return 1 if os.path.isdir(path) else 0
        file_df['dir'] = file_df['path'].apply(isdir)

        file_df = file_df.sort_values(['dir', 'name'], ascending=[False, True]).reset_index(drop=True)
        self.file_df = file_df
        


    def parse_image_file(self, parse_dir, force=False):
        """ Update current image df.
        image df: 
        - id:  image id (basename or sereies description)
        - path: image/series files
        - type: indicate how to open file
        """
        if (parse_dir == self.parse_dir) and not force: 
            return

        self.parse_dir = parse_dir # cache parsed dir

        df_make = lambda _id, path, _type : pd.DataFrame({
            "id":_id, 
            "path":path, 
            "type":[_type]*len(_id),
            })
        # parse for file
        ## dcm 
        dcm_path = glob.glob(os.path.join(parse_dir, '*.dcm'))
        if len(dcm_path) > 0 : 
            dcm_serie_desc, dcm_path = Utils.parse_dcm_file(dirname=parse_dir)
            dcm_df = df_make(dcm_serie_desc, dcm_path, 'dcm')

        else: 
            dcm_df = df_make([], [], None)

        ## rgb image

        ## np.loadable
        npy_path = glob.glob(os.path.join(parse_dir, '*.npy'))
        npy_df = df_make(_id=[os.path.basename(path) for path in npy_path ], 
                path=npy_path, 
                _type='npy')

        npz_path = glob.glob(os.path.join(parse_dir, '*.npz'))
        npz_df = df_make(_id=[os.path.basename(path) for path in npz_path ], 
                path=npz_path, 
                _type='npz')

        ## Sitk Readable; mha/nii.gz
        sitk_path = [str(p.resolve()) for p in pathlib.Path(parse_dir).glob("*") 
                if "".join(p.suffixes) in Utils.sitk_readable_ext]

        # mha_path = glob.glob(os.path.join(parse_dir, '*.mha'))
        # nii_path = glob.glob(os.path.join(parse_dir, '*.nii*'))
        # sitk_path = mha_path + nii_path
        sitk_df = df_make(_id=[os.path.basename(path) for path in sitk_path], 
                path=sitk_path,
                _type='sitk',
                )

        ## vtk
        # vtk_path = glob.glob(os.path.join(parse_dir, '*.vtk'))
        # vtk_df = df_make(_id=[os.path.basename(path) for path in vtk_path], 
        #         path=vtk_path,
        #         _type='vtk')
        self.image_df = pd.concat([sitk_df, npy_df, npz_df, dcm_df,],
                ignore_index=True,)
        self.image_df = self.image_df.sort_values(['id']).reset_index(drop=True)

