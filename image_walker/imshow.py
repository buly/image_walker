import os
import re
import pdb
import tempfile
import zipfile

import dash 
import dash_table
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State, ALL, MATCH
# import dash_vtk
# from dash_vtk.utils import presets
# from dash_vtk.utils import to_mesh_state
from dash.exceptions import PreventUpdate
from dash_extensions import Download
from dash_extensions.snippets import send_file

import plotly
import plotly.express as px
import plotly.graph_objects as go

import numpy as np
import SimpleITK as sitk
import scipy.ndimage as spn

from . import utils as Utils


class Imshow(object):
    """
    """
    def __init__(self, 
            # app=None,
            image=None,
            spacing=None,
            title=None,
            col_wrap=None,
            path=None,
            *args, **kwargs):
        """
        """
        # super().__init__(*args, **kwargs)
        self.build()
        self.update(image=image, 
                spacing=spacing, 
                title=title, 
                path=path)


    def deploy(self, app=None, title='Imshow'):
        """ Deploy callbakc to app
        """
        self.app = app
        if self.app is None:
            external_stylesheets = [dbc.themes.BOOTSTRAP]
            self.app = dash.Dash(__name__, 
                    external_stylesheets=external_stylesheets, 
                    suppress_callback_exceptions=True,
                    title=title,
                    )
            self.app.layout = self.layout


        @self.app.callback(
                Output('img-slice-slider', 'max'),
                Output('img-slice-slider', 'value'),
                Input('img-axis', 'value'),
                )
        def update_image_axis(axis_nb):
            """
            """
            if not self.image:
                raise PreventUpdate

            if self.image[0].ndim == 4:
                _max = max([1] + [_img.shape[axis_nb+1] - 1  for _img in self.image])
                _val = _max // 2
            else:
                _max = max([1] + [_img.shape[axis_nb] - 1 for _img in self.image])
                _val = _max // 2
            return _max, _val


        @self.app.callback(
                 Output('img-graph', 'figure'),

                Input('img-selector', 'value'),
                Input('img-slice-slider', 'value'),
                Input('img-val-slider', 'value'),
                Input('img-slice-preprocess', 'value'),

                State('viewer-col', 'value'),
                State('img-view-selector', 'value'),
                # State('img-viewer-mode', 'children'),
                State('img-axis', 'value'),
                State('img-selector', 'options'),
                )
        def update_image_viewer(img_nb, 

                slice_nb, 
                intensity_range, 
                slice_preprocess,
                n_col,

                img_viewer_mode,
                axis_nb,
                img_options,
                ):
            """
            """
            if (len(self.image) == 0) or (len(img_nb) == 0) : 
                raise PreventUpdate

            z_min, z_max = intensity_range
            _img = self.get_image_slice(img_nb=img_nb, 
                    slice_nb=slice_nb, 
                    axis_nb=axis_nb,
                    zmax=z_max, 
                    superimpose=img_viewer_mode == 'superimpose',
                    )
            img_title = [img_options[ind]['label'] for ind in img_nb]
            title = '.'.join(img_title) if img_viewer_mode == 'superimpose' else img_title
            n_col = n_col if img_viewer_mode != 'superimpose' else None

            for item in slice_preprocess: 
                if item == 'vflip': 
                    if isinstance(_img, (list, tuple)):
                        _img = [__img[..., ::-1] for __img in _img]
                    else:
                        _img = _img[..., ::-1]

                elif item == 'hflip':
                    if isinstance(_img, (list, tuple)):
                        _img = [__img[::-1, :] for __img in _img]
                    else:
                        _img = _img[::-1, :]

            fig = get_image_fig(_img, 
                    title=title,
                    zmax=z_max, 
                    zmin=z_min,
                    n_col=n_col,
                    )
            return fig


        @self.app.callback(
                Output('img-graph-loading', 'children'),
                Input('refresh-image-button', 'n_clicks'),
                Input('img-view-selector', 'value'),
                Input('viewer-col', 'value'),
                )
        def update_image_graph(click, 
                viewer_mode,
                n_col):
            """
            """
            children=dcc.Graph(id='img-graph',
                        style=dict(
                            height='80vh',
                            minWidth='50vw'
                            )
                        )
            return children

        @self.app.callback(
                Output('download-image', 'data'),
                Input('download-image-button', 'n_clicks'),
                # Input('img-view-selector', 'value'),
                # Input('viewer-col', 'value'),
                )
        def download_image_fn(click,):
            """
            """
            ctx = dash.callback_context
            button_triggered = False if not ctx.triggered\
                    else ctx.triggered[0]['prop_id'].split('.')[0] == 'download-image-button'
            if button_triggered:
                if len(self.path) == 1: 
                    path = self.path[0]
                    return send_file(path)

                else: 
                    with tempfile.NamedTemporaryFile(suffix='.zip', prefix='pkg-') as tmp:
                        with zipfile.ZipFile(tmp.name, 'w') as zip:
                            for path in self.path: 
                                zip.write(path, os.path.basename(path))
                        return send_file(tmp.name)


    def get_image_slice(self, 
            img_nb, 
            slice_nb, 
            axis_nb, 
            zmax=None, 
            superimpose=True,
            ):
        """
        """
        imgs = []
        count = 0

        # extract image slices
        for ind in img_nb: 
            _img = np.copy(self.image[ind])
            _spacing = self.spacing[ind].copy()
            _shape = _img.shape
            _slice_nb = slice_nb 
            if _slice_nb >= _shape[axis_nb]:
                _slice_nb = _shape[axis_nb] - 1
            if _img.ndim == 3:
                _img = _img.take(_slice_nb, axis=axis_nb)
                _spacing.pop(axis_nb)

            if _img.ndim == 4: # use the first dimension as animation
                _img = _img.take(_slice_nb, axis=axis_nb+1)
                _spacing.pop(0) # ignore time dim
                _spacing.pop(axis_nb)

            if ~np.all([item==_spacing[0] for item in _spacing]):
                min_spacing = np.min(_spacing)
                _multiplier = np.array(_spacing)/np.array(min_spacing)
                if _img.ndim == 3:
                    _multiplier = np.insert(_multiplier, 0, 1)
                _img = spn.zoom(_img, zoom=_multiplier)
            imgs.append(_img.astype(float))
            if np.sum(_img) > 0:
                count += 1

        alpha = 1/count if count != 0 else 1

        # regularized according to max intensity
        ## combine image if superimpose
        if len(img_nb) > 1:
            if zmax is None:
                zmax = np.max(imgs)
            img = []
            for _img in imgs:
                if np.sum(_img) > 0:
                    if _img.max() < zmax: 
                        _img = zmax * (_img/_img.max())
                    if superimpose:
                        img.append(alpha * _img)
                    else: 
                        img.append(_img)
                else:
                    img.append(_img)
            if superimpose: 
                img = np.sum(img, axis=0)
        else: 
            img = imgs[0]
        return img


    def update(self,
            image=None, 
            title=None, 
            spacing=None, 
            path=None,
            ):
        """
        """
        self.path = path
        if (image is None) or (len(image) == 0):
            self.image = []
            self.title = []
            self.spacing = []

        else:
            if not isinstance(image, (list, tuple)):
                image = [image]
            self.image = image

            self.title = title
            if self.title is None:
                self.title = [f"img-{ind}" for ind in range(len(self.image))]
            elif not isinstance(self.title, (list, tuple)):
                self.title = [self.title]

            self.spacing = spacing
            if self.spacing is None: 
                self.spacing = [[1]*_img.ndim for _img in self.image] # [1] * self.image.ndim

            elif not isinstance(self.spacing[0], (list, tuple)):
                self.spacing = [self.spacing]

            # update layout component
            # add change dropdown 
            self.img_selector.options = [{'label': key, 'value': ind} for ind, key in enumerate(self.title)]
            self.img_selector.value = list(range(len(self.title)))

            zmin = min([_img.min() for _img in self.image])
            zmax = max([_img.max() for _img in self.image])
            self.val_slider.min = zmin
            self.val_slider.max = zmax
            self.val_slider.value = [zmin, zmax]
            step = (zmax - zmin)/100.0
            self.val_slider.step = step if step < 1 else 1


    def build(self,): 
        """  Init with empty image.
        """
        img = []
        title = []

        self.img_selector = dcc.Dropdown(id='img-selector',
                options=[{'label':key, 'value': ind} for ind, key in enumerate(title)],
                value=list(range(len(title))),
                multi=True,
                )

        self.img_view_selector = dcc.Dropdown(id='img-view-selector',
                options=[{'label':key, 'value': key} for key in ['multi', 'superimpose']],
                value='multi',
                persistence=True,
                )

        self.viewer_col = dbc.Input(id='viewer-col', 
                type='number',
                min=1,
                max=10,
                step=1,
                value=None,
                persistence=True,
                )

        self.refresh_image_button = dbc.Button("↻",
                                    id='refresh-image-button',
                                    color='success',
                                    outline=True,
                                    block=True,
                                    )

        self.download_image_button = dbc.Button("⤋",
                                    id='download-image-button',
                                    color='success',
                                    outline=False,
                                    block=True,
                                    )

        self.download_image = Download(id='download-image',)

        self.img_viewer_mode = html.Div(id='img-viewer-mode',
                children='superimpose',
                style=dict(display='none'),
                )

        preprocess = ['vflip', 'hflip']
        self.img_slice_preprocess = dcc.Dropdown(id='img-slice-preprocess',
                options=[{'label': key, 'value':key} for key in preprocess],
                value=[],
                multi=True,
                clearable=True,
                persistence=False,
                )

        self.axer = dcc.Dropdown(id="img-axis",
                value=0, 
                options=[{"label": key, "value": ind}
                    for ind, key in enumerate(['0', '1', '2'])],
                clearable=True,
                persistence=True,
                )
        # self.axer_tooltip = dbc.Tooltip(
        #         'Image Axis',
        #         target='img-axis', 
        #         )

        self.slider = dcc.Slider(id='img-slice-slider',
                min=0,
                max=1,
                step=1,
                value=0,
                tooltip={
                        'always_visible':True,
                        'placement':'top',
                    },
                persistence=True,
                )

        zmin = min([0] + [_img.min() for _img in img])
        zmax = max([1] + [_img.max() for _img in img])
        self.val_slider = dcc.RangeSlider(id="img-val-slider",
            min=zmin, 
            max=zmax,
            tooltip={
                    'always_visible':True,
                    'placement':'top',
                },
            value=[zmin, zmax],
            )

        self.main_viewer = dcc.Loading(
                    id='img-graph-loading',
                    type='circle',
                    children=dcc.Graph(id='img-graph',
                        style=dict(
                            height='80vh',
                            minWidth='50vw'
                            ),
                        ),
                )
        
        self.layout = dbc.Container(
                children=[
                    dbc.Row(
                        children=[
                            dbc.Col(html.Div("Img:"), 
                                width=dict(size=2),
                                ),
                            dbc.Col(
                                html.Div(
                                    self.img_selector,
                                    ),
                                width=dict(size=5),
                                ),
                            dbc.Col(
                                html.Div(
                                    self.img_view_selector,
                                    ),
                                width=dict(size=3),
                                ),
                            dbc.Col(
                                self.refresh_image_button,
                                width=dict(size=1),
                                ),
                            dbc.Col(
                                dbc.Spinner(
                                    [self.download_image_button, self.download_image,],
                                    size='sm', 
                                    ),
                                width=dict(size=1),
                                ),
                            ]
                        ),
                    dbc.Row(
                        children=[
                            dbc.Col(html.Div("Ax./Flip/Col:"),
                                width=dict(size=2),
                                ),
                            dbc.Col(
                                html.Div(
                                    self.axer,# self.axer_tooltip],
                                    style=dict(width='100%')
                                    ),
                                width=dict(size=2),
                                ),
                            dbc.Col(
                                html.Div(self.img_slice_preprocess, 
                                    style=dict(width='100%')
                                    ),
                                width=dict(size=6),
                                ),
                            dbc.Col(
                                html.Div(self.viewer_col, 
                                    style=dict(width='100%')
                                    ),
                                width=dict(size=2),
                                ),
                            # dbc.Col(
                            #     html.Div(self.col_wrap, 
                            #         style=dict(width='100%')
                            #         ),
                            #     width=dict(size=1),
                            #     ),
                            ],
                        ),
                    dbc.Row(
                        children=[
                            dbc.Col(html.Div("Slice/Val:"),
                                width=dict(size=2),
                                ),
                            dbc.Col(
                                html.Div(self.slider, 
                                    style=dict(width='100%')
                                    ),
                                ),
                            dbc.Col(
                                children=[
                                    self.img_viewer_mode,
                                    html.Div(self.val_slider, 
                                        style=dict(width='100%')
                                        ),
                                    ],
                                ),
                            ],
                        ),
                    dbc.Row(
                        html.Div(self.main_viewer,
                            style=dict(width='100%')
                            )
                        ),
                    ],
                )

def get_image_fig(img_slice,
        title,
        zmax,
        zmin,
        axis=0,
        n_col=None
        ):
    """
    """
    if not isinstance(img_slice, (list, tuple)):
        img_slice = [img_slice]

    if not isinstance(title, (list, tuple)):
        title = [title]

    img_slice = np.array(img_slice)
    animation_frame = 1 if img_slice.ndim == 4 else None

    fig = px.imshow(img_slice,
            facet_col=0,
            facet_col_wrap=n_col,
            zmax=zmax,
            zmin=zmin,
            aspect="equal", 
            color_continuous_scale="gray", 
            binary_string=True,
            animation_frame=animation_frame,
            )
    fig.for_each_annotation(lambda a: a.update(text=title[int(a.text.split('=')[-1])]))
    return fig

