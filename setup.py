from setuptools import setup

# get long description from 
# with open("README.md", 'r') as f: 
#     long_description = f.read()

setup(
    name='image_walker',
    version='0.0.1',
    author='Buntheng LY',
    packages=["image_walker"],
    # long_description=long_description,
    long_description_content_type="text/markdown",
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent"
        ],
    entry_points={
        'console_scripts': [
            'imwalker=image_walker:cli.cli_imwalker',
            'walker=image_walker:cli.cli_walker',
            ],
    },
    python_requires='>=3.6',
    # install_requires=[
    #     'SimpleITK', 
    #     'numpy', 
    #     'pandas', 
    #     ]
)
